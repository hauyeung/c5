#include<stdio.h>
#include<stdlib.h>
float my_min(float x, float y);
int main(void)
{
	float num1, num2;
	char s1[256], s2[256];
	printf("Please enter 2 numbers: ");
	fgets(s1,256,stdin);
	fgets(s2,256,stdin);
	num1 = atof(s1);
	num2 = atof(s2);
	float n = my_min(num1,num2);
	printf("The smaller number is: %f.", n);
}

float my_min(float x, float y)
{
	if (x < y)
	{
		return x;
	}
	else 
	{
		return y;
	}
}